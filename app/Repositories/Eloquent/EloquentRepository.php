<?php


namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Contracts\Repositories\EloquentRepository as IEloquentRepository;

abstract class EloquentRepository implements IEloquentRepository
{
    public function model(): string
    {
        return Model::class;
    }

    public function baseQuery(): Builder
    {
        return $this
            ->model()
            ::query();
    }

    public function all(): Collection
    {
        return $this
            ->model()
            ::all();
    }

    public function find(int $id)
    {
        return $this->model()::find($id);
    }
}
