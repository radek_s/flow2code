<?php

namespace App\Http\Requests\Api\Movies;

use Illuminate\Foundation\Http\FormRequest;

class IndexMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * Its a good practice to make use of Gates/Policies here, ie:
         * return $this->user('api')->can(MoviePolicy::INDEX, Movie::class)
         *
         * For testing purposes I'm allowing anybody to CRUD movies
         */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
