<?php


namespace App\Repositories\Eloquent;

use App\Contracts\Models\Model;
use App\Contracts\Models\MovieModel;
use App\Movie;
use App\Contracts\Dtos\Dto;
use App\Repositories\Eloquent\EloquentRepository as AbstractEloquentRepository;
use App\Contracts\Repositories\MoviesRepository as IMoviesRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class MoviesRepository extends AbstractEloquentRepository implements IMoviesRepository
{
    /**
     * @inheritDoc
     */
    public function model(): string
    {
        return Movie::class;
    }

    /**
     * @inheritDoc
     */
    public function update(Model $model, Dto $dto): MovieModel
    {
        $model->update([
            'title' => $dto->getTitle(),
            'description' => $dto->getDescription(),
            'country_id' => $dto->getCountryId()
        ]);

        $model
            ->genres()
            ->sync($dto->getGenres());

        if($dto->hasCover()) {
            $model
                ->cover()
                ->updateOrCreate(
                    [
                        'coverable_id' => $model->id,
                        'coverable_type' => 'movie'
                    ],
                    [
                        'path' => $dto->getCover()
                    ]
                );
        }

        return $model
            ->refresh()
            ->load([
                'genres',
                'country',
                'cover'
            ]);
    }

    /**
     * @inheritDoc
     */
    public function create(Dto $dto): MovieModel
    {
        $movie = $this
            ->model()
            ::create([
                'title' => $dto->getTitle(),
                'description' => $dto->getDescription(),
                'country_id' => $dto->getCountryId()
            ]);

        $movie
            ->genres()
            ->attach($dto->getGenres());

        if($dto->hasCover()) {
            $movie
                ->cover()
                ->create([
                    'path' => $dto->getCover()
                ]);
        }

        return $movie;
    }

    /**
     * @inheritDoc
     */
    public function destroy(Model $model): void
    {
        /**
         * Equivalent to $this->repository->destroy($model->id),
         * but since Eloquent allows shorthand method
         * we're gonna go with this
         */
        $model
            ->delete();
    }

    /**
     * @param string $phrase
     * @return Collection
     */
    public function byTitle(string $phrase): Collection
    {
        return $this
            ->baseQuery()
            ->whereRaw("LOWER(title) LIKE ?", ['%' . $phrase . '%'])
            ->get();
    }
}
