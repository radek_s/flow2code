<?php

namespace App\Providers;

use App\Contracts\Models\{CoverModel, MovieModel};
use App\Cover;
use App\Movie;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use App\Contracts\Repositories\{
    MoviesRepository as IMoviesRepository,
    CoversRepository as ICoversRepository
};
use App\Repositories\Eloquent\{
    CoversRepository,
    MoviesRepository
};

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IMoviesRepository::class, MoviesRepository::class);
        $this->app->bind(ICoversRepository::class, CoversRepository::class);

        $this->app->bind(MovieModel::class, Movie::class);
        $this->app->bind(CoverModel::class, Cover::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::$morphMap = [
            'movie' => Movie::class
        ];
    }
}
