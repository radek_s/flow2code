<?php


namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use App\Contracts\Models\{Model, CoverModel};
use App\Cover;
use App\Contracts\Dtos\Dto;
use App\Repositories\Eloquent\EloquentRepository as AbstractEloquentRepository;
use App\Contracts\Repositories\CoversRepository as ICoversRepository;

class CoversRepository extends AbstractEloquentRepository implements ICoversRepository
{
    /**
     * @inheritDoc
     */
    public function model(): string
    {
        return Cover::class;
    }

    /**
     * @inheritDoc
     */
    public function update(Model $model, Dto $dto): CoverModel
    {
//        $model
//            ->update([
//                'path' => $dto->getPath()
//            ]);
//
//        return $model
//            ->refresh()
//            ->load([
//                'movie'
//            ]);
    }

    /**
     * @inheritDoc
     */
    public function create(Dto $dto): CoverModel
    {
//        $cover = $this
//            ->model()
//            ::create([
//                'path' => $dto->getPath()
//            ]);
//
//        return $cover;
    }

    /**
     * @inheritDoc
     */
    public function destroy(Model $model): void
    {
        $model
            ->delete();
    }
}
