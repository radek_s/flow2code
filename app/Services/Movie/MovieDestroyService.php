<?php


namespace App\Services\Movie;

use App\Contracts\Models\MovieModel;
use App\Contracts\Repositories\MoviesRepository;
use App\Events\Movie\MovieDeleted;

class MovieDestroyService
{
    /**
     * @var MoviesRepository
     */
    private MoviesRepository $repository;

    /**
     * MovieDestroyService constructor.
     * @param MoviesRepository $repository
     */
    public function __construct(MoviesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function destroy(MovieModel $model): void
    {
        event(new MovieDeleted($model));

        $this
            ->repository
            ->destroy($model);
    }

}
