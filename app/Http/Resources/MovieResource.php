<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MovieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'genres' => GenreResource::collection($this->whenLoaded('genres')),
            'country' => CountryResource::make($this->whenLoaded('country')),
            'cover' => CoverResource::make($this->whenLoaded('cover'))
        ];
    }
}
