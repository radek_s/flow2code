<?php

/** @var Factory $factory */

use App\Country;
use App\Movie;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Movie::class, function (Faker $faker) {

    $country = Country::inRandomOrder()->first();

    return [
        'title' => $faker->sentence(2),
        'description' => $faker->sentence(10),
        'country_id' => $country ? $country->id : factory(Country::class)->create()->id
    ];
});
