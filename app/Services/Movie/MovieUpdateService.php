<?php


namespace App\Services\Movie;


use App\Contracts\Models\MovieModel;
use App\Contracts\Repositories\MoviesRepository;
use App\Dto\Movie\Movie as Dto;
use App\Events\Movie\MovieUpdated;

class MovieUpdateService
{
    /**
     * @var MoviesRepository
     */
    private MoviesRepository $repository;

    /**
     * MovieUpdateService constructor.
     * @param MoviesRepository $repository
     */
    public function __construct(MoviesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param MovieModel $movie
     * @param Dto $dto
     * @fire MovieUpdated
     * @return MovieModel
     */
    public function update(MovieModel $movie, Dto $dto): MovieModel
    {
        $updated = $this
            ->repository
            ->update($movie, $dto);

        event(new MovieUpdated($updated));

        return $updated;
    }
}
