<?php


namespace App\Dto\Movie;

use App\Contracts\Dtos\Dto;

class Movie implements Dto
{
    private string $title;

    private ?string $description;

    private int $countryId;

    private array $genres;

    private ?string $cover;

    /**
     * Movie constructor.
     * @param string $title
     * @param int $countryId
     * @param array $genres
     * @param string|null $description
     * @param string|null $cover
     */
    public function __construct(
        string $title,
        int $countryId,
        array $genres,
        ?string $description = null,
        ?string $cover = null
    )
    {
        $this->title = $title;
        $this->countryId = $countryId;
        $this->genres = $genres;
        $this->description = $description;
        $this->cover = $cover;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @param int $countryId
     */
    public function setCountryId(int $countryId): void
    {
        $this->countryId = $countryId;
    }

    /**
     * @return array
     */
    public function getGenres(): array
    {
        return $this->genres;
    }

    /**
     * @param array $genres
     */
    public function setGenres(array $genres): void
    {
        $this->genres = $genres;
    }

    /**
     * @return string|null
     */
    public function getCover(): ?string
    {
        return $this->cover;
    }

    /**
     * @param string|null $cover
     */
    public function setCover(?string $cover): void
    {
        $this->cover = $cover;
    }

    /**
     * @return bool
     */
    public function hasCover(): bool
    {
        return ($this->cover !== null);
    }
}
