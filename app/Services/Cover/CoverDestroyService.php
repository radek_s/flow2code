<?php


namespace App\Services\Cover;


use App\Contracts\Models\CoverModel;
use App\Contracts\Repositories\CoversRepository;
use Illuminate\Support\Facades\Storage;

class CoverDestroyService
{
    /**
     * @var CoversRepository
     */
    private CoversRepository $repository;

    /**
     * CoverDestroyService constructor.
     * @param CoversRepository $repository
     */
    public function __construct(CoversRepository $repository)
    {
        $this->repository = $repository;
    }

    public function destroy(CoverModel $model, bool $removeFile = false): void
    {
        if($removeFile) {
            Storage
                ::delete($model->path);
        }

        $this
            ->repository
            ->destroy($model);
    }
}
