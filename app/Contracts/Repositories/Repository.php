<?php


namespace App\Contracts\Repositories;

use App\Contracts\Dtos\Dto;
use App\Contracts\Models\Model;
use Illuminate\Support\Collection;

interface Repository
{
    /**
     * @return Collection
     */
    public function all(): Collection;

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id);

    /**
     * @param Dto $dto
     * @return Model
     */
    public function create(Dto $dto): Model;

    /**
     * @param Model $model
     * @param Dto $dto
     * @return Model
     */
    public function update(Model $model, Dto $dto): Model;

    /**
     * @param Model $model
     */
    public function destroy(Model $model): void;
}
