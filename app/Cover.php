<?php

namespace App;

use App\Contracts\Models\CoverModel;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\Storage;

class Cover extends Model implements CoverModel
{
    use HasTimestamps;

    protected $with = [];

    protected $fillable = [
        'path'
    ];

    protected $appends = [
        'url'
    ];

    protected $hidden = [
        'path',
        'coverable_id',
        'coverable_type',
        'created_at',
        'updated_at'
    ];

    public static function storagePath()
    {
        return config('app.cover.dir');
    }

    public function getUrlAttribute($value): string
    {
        return url("/storage/{$this->path}");
    }

    public function getFilePathAttribute()
    {
        return public_path("/storage/{$this->path}");
    }

    public function coverable(): MorphTo
    {
        return $this->morphTo('coverable');
    }
}
