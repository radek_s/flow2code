<?php


namespace App\Repositories\Memory;

use App\Contracts\Dtos\Dto;
use App\Contracts\Models\Model;
use App\Contracts\Models\MovieModel;
use App\Contracts\Repositories\MoviesRepository as IMoviesRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class MoviesRepository implements IMoviesRepository
{
    private Collection $items;

    /**
     * MoviesRepository constructor.
     */
    public function __construct(Collection $items)
    {
        $this->items = $items;
    }

    public function all(): Collection
    {
        // TODO: Implement all() method.
    }

    public function find(int $id)
    {
        // TODO: Implement find() method.
    }

    public function create(Dto $dto): Model
    {
        // TODO: Implement create() method.
    }

    public function update(Model $model, Dto $dto): Model
    {
        // TODO: Implement update() method.
    }

    public function destroy(Model $model): void
    {
        // TODO: Implement destroy() method.
    }

    public function byTitle(string $phrase): Collection
    {
        return $this
            ->items
            ->filter(function (MovieModel $item) use ($phrase) {
                return Str::contains($item->title, $phrase);
            });
    }
}
