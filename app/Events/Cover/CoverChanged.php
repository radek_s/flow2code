<?php

namespace App\Events\Cover;

use App\Contracts\Models\CoverModel;
use App\Dto\Cover\Resize;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CoverChanged
{
    use Dispatchable, SerializesModels;

    private CoverModel $cover;

    private Resize $resizeParams;

    /**
     * CoverChanged constructor.
     * @param CoverModel $cover
     * @param Resize $resizeParams
     */
    public function __construct(CoverModel $cover, Resize $resizeParams)
    {
        $this->cover = $cover;
        $this->resizeParams = $resizeParams;
    }

    /**
     * @return Resize
     */
    public function getResizeParams(): Resize
    {
        return $this->resizeParams;
    }

    /**
     * @return CoverModel
     */
    public function getCover(): CoverModel
    {
        return $this->cover;
    }
}
