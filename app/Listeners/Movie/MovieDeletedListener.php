<?php

namespace App\Listeners\Movie;

use App\Events\Movie\MovieDeleted;
use App\Services\Cover\CoverDestroyService;

class MovieDeletedListener
{
    /**
     * @var CoverDestroyService
     */
    private CoverDestroyService $service;

    /**
     * Create the event listener.
     *
     * @param CoverDestroyService $service
     * @return void
     */
    public function __construct(CoverDestroyService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(MovieDeleted $event)
    {
        if($event->getMovie()->hasCover()) {
            $this
                ->service
                ->destroy(
                    $event->getMovie()->cover,
                    true
                );

        }
    }
}
