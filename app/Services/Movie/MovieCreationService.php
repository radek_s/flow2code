<?php


namespace App\Services\Movie;


use App\Contracts\Models\MovieModel;
use App\Contracts\Repositories\MoviesRepository;
use App\Dto\Cover\Resize;
use App\Dto\Movie\Movie as Dto;
use App\Events\Cover\CoverChanged;
use App\Events\Movie\MovieCreated;
use App\Services\Cover\CoverResizeService;

class MovieCreationService
{
    /**
     * @var MoviesRepository
     */
    private MoviesRepository $repository;

    /**
     * MovieCreationService constructor.
     * @param MoviesRepository $repository
     */
    public function __construct(MoviesRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \App\Dto\Movie\Movie $dto
     * @return MovieModel
     * @fire MovieCreated
     * @fire CoverChanged
     */
    public function create(Dto $dto): MovieModel
    {
        $movie = $this
            ->repository
            ->create($dto);

        event(new MovieCreated($movie));

        return $movie
            ->load([
                'genres',
                'country',
                'cover'
            ]);
    }
}
