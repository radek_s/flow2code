<?php

namespace App\Events\Movie;

use App\Contracts\Models\MovieModel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MovieCreated
{
    use Dispatchable, SerializesModels;

    /**
     * @var MovieModel
     */
    private MovieModel $movie;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MovieModel $movie)
    {
        $this->movie = $movie;
    }

    /**
     * @return MovieModel
     */
    public function getMovie(): MovieModel
    {
        return $this->movie;
    }
}
