<?php


namespace App\Services\Movie;


use App\Contracts\Repositories\MoviesRepository;
use App\Dto\Movie\Search;
use Illuminate\Support\Collection;

class MovieSearchService
{
    /**
     * @var MoviesRepository
     */
    private MoviesRepository $repository;

    /**
     * MovieSearchService constructor.
     * @param MoviesRepository $repository
     */
    public function __construct(MoviesRepository $repository)
    {
        $this->repository = $repository;
    }

    public function search(Search $dto): Collection
    {
        return $this
            ->repository
            ->byTitle($dto->getLowerPhrase());
    }
}
