<?php


namespace App\Contracts\Repositories;


use Illuminate\Database\Eloquent\Builder;

interface EloquentRepository extends Repository
{
    /**
     * @return string
     */
    public function model(): string;

    /**
     * @return Builder
     */
    public function baseQuery(): Builder;
}
