<?php

namespace App\Http\Requests\Api\Movies;

use Illuminate\Foundation\Http\FormRequest;

class DestroyMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * $this->user('api')->can(MoviePolicy::DESTROY, $this->route('movie'))
         */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
