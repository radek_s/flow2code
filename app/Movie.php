<?php

namespace App;

use App\Contracts\Models\MovieModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Str;

class Movie extends Model implements MovieModel
{
    use HasTimestamps;

    protected $with = [
        'genres',
        'country',
        'cover'
    ];

    protected $fillable = [
        'movie_id',
        'title',
        'description',
        'country_id'
    ];

    public function genres(): BelongsToMany
    {
        return $this
            ->belongsToMany(Genre::class, 'movies_genres')
            ->withTimestamps();
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    public function cover(): MorphOne
    {
        return $this->morphOne(Cover::class, 'coverable');
    }

    public function scopeByGenre(Builder $query, string $genre): Builder
    {
        return $query->whereHas('genres', static function(Builder $query) use ($genre) {
            $query->where('name', $genre);
        });
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function(Movie $movie) {
            $movie->movie_id = Str::random(6);
        });
    }

    public function hasCover(): bool
    {
        return $this
            ->cover()
            ->exists();
    }
}
