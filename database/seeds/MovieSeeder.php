<?php

use App\Country;
use App\Genre;
use App\Movie;
use Illuminate\Database\Seeder;

class MovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Movie::class, 50)
            ->create()
            ->each(function (Movie $movie) {

                $genres = Genre::inRandomOrder()->take(3);

                $movie
                    ->genres()
                    ->attach(
                        $genres->pluck('id')
                    );

            });
    }
}
