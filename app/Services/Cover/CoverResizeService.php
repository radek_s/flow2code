<?php


namespace App\Services\Cover;

use App\Dto\Cover\Resize;
use Intervention\Image\Facades\Image;

class CoverResizeService
{
    public function resize(
        string $path,
        Resize $resize,
        ?string $destination = null
    ): void
    {
        Image
            ::make($path)
            ->resize(
                $resize->getWidth(),
                $resize->getHeight(),
                $resize->getAspectRatioFunction()
            )
            ->save($destination);
    }
}
