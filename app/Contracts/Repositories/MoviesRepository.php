<?php


namespace App\Contracts\Repositories;

use Illuminate\Support\Collection;

interface MoviesRepository extends Repository
{
    public function byTitle(string $phrase): Collection;
}
