<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    private array $devSeeders = [
        CountrySeeder::class,
        GenreSeeder::class,
        MovieSeeder::class
    ];

    private array $prodSeeders = [];

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        switch(config('app.env')) {

            case 'local':
            case 'develop':
            case 'testing':
            case 'staging':
                $this->call($this->devSeeders);
                break;

//            case 'production':
//                $this->call($this->prodSeeders);
//                break;

        }
    }
}
