<?php

namespace App\Http\Requests\Api\Movies;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMovieRequest extends StoreMovieRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * $this->user('api')->can(MoviePolicy::UPDATE, $this->route('movie'))
         */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return parent::rules();
    }
}
