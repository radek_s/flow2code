<?php

namespace App\Listeners\Cover;

use App\Events\Cover\CoverChanged;
use App\Services\Cover\CoverResizeService;

class CoverChangedListener
{
    /**
     * @var CoverResizeService
     */
   private CoverResizeService $service;

    /**
     * CoverChangedListener constructor.
     * @param CoverResizeService $service
     */
    public function __construct(CoverResizeService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param  CoverChanged  $event
     * @return void
     */
    public function handle(CoverChanged $event)
    {
        $this
            ->service
            ->resize(
                $event->getCover()->filePath,
                $event->getResizeParams()
            );
    }
}
