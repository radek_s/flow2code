# Environment

[Laradock](https://laradock.io/) with following images:
 * Apache2
 * MySQL
 * PHPMyAdmin
 * Redis

Requires PHP 7.4 (project uses [property types](https://stitcher.io/blog/typed-properties-in-php-74)):
* composer.json already requires 7.4
* Laradock .env file needs to be properly adjusted 

# How to run
`cd path/to/laradock/root && docker-compose up -d --build apache2 mysql phpmyadmin redis`

`docker-compose exec --user=laradock workspace bash`

`laradock@docker: cd /path/to/project`

`php artisan migrate:fresh --seed`

Database main seeder has been configured to fill in the database depending on environment used (default: `local` see: `.env` file)

Please note that apache vhost needs to be configured (`laradock/root/apache2/sites/*`)

# Test
`php artisan test` - runs simple feature test
