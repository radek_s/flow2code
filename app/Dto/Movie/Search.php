<?php


namespace App\Dto\Movie;

class Search
{
    /**
     * @var string
     */
    private string $phrase;

    /**
     * Search constructor.
     * @param string $phrase
     */
    public function __construct(string $phrase)
    {
        $this->phrase = $phrase;
    }

    /**
     * @return string
     */
    public function getPhrase(): string
    {
        return $this->phrase;
    }

    public function getLowerPhrase()
    {
        return strtolower($this->getPhrase());
    }
}
