<?php

namespace App\Http\Requests\Api\Movies;

use App\Dto\Movie\Search;
use Illuminate\Foundation\Http\FormRequest;

class SearchMoviesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phrase' => 'required|string|min:3'
        ];
    }

    public function dto(): Search
    {
        return new Search(
            $this->get('phrase')
        );
    }
}
