<?php

namespace App\Http\Requests\Api\Movies;

use App\Cover;
use App\Dto\Cover\Resize;
use App\Dto\Movie\Movie;
use Illuminate\Foundation\Http\FormRequest;

class StoreMovieRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * $this->user('api')->can(MoviePolicy::STORE, Movie::class);
         */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'nullable|string|min:5',
            'country' => 'required|exists:countries,id',
            'genres' => 'required|array|min:1',
            'genres.*' => 'exists:genres,id',
            'cover' => 'nullable|image'
        ];
    }

    public function dto(): Movie
    {
        $cover = $this->hasFile('cover')
            ? $this->file('cover')
                ->store(Cover::storagePath())
            : null;

        return new Movie(
            $this->input('title'),
            $this->input('country'),
            $this->input('genres'),
            $this->input('description'),
            $cover
        );
    }

    public function resize(): Resize
    {
        /**
         * Can be parametrized with inputs
         */
        return new Resize(
            config('app.cover.default_width'),
            config('app.cover.default_height'),
            config('app.cover.aspect_ratio')
        );
    }
}
