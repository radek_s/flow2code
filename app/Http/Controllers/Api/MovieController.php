<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Repositories\MoviesRepository;
use App\Events\Cover\CoverChanged;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Movies\{DestroyMovieRequest,
    IndexMovieRequest,
    SearchMoviesRequest,
    StoreMovieRequest,
    UpdateMovieRequest};
use App\Http\Resources\MovieResource;
use App\Movie;
use App\Services\Movie\{MovieCreationService, MovieDestroyService, MovieSearchService, MovieUpdateService};
use Illuminate\Http\JsonResponse;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexMovieRequest $request
     * @param MoviesRepository $repository
     * @return JsonResponse
     */
    public function index(IndexMovieRequest $request, MoviesRepository $repository): JsonResponse
    {
        return new JsonResponse(
            MovieResource::collection($repository->all())
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMovieRequest $request
     * @param MovieCreationService $service
     * @return JsonResponse
     */
    public function store(StoreMovieRequest $request, MovieCreationService $service): JsonResponse
    {
        $movieDto = $request->dto();

        $movie = $service
            ->create(
                $movieDto,
            );

        if($movieDto->hasCover()) {
            event(new CoverChanged($movie->cover, $request->resize()));
        }

        return new JsonResponse(
            MovieResource::make($movie)
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Movie $movie
     * @return JsonResponse
     */
    public function show(Movie $movie): JsonResponse
    {
        return new JsonResponse(
            MovieResource::make($movie)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMovieRequest $request
     * @param Movie $movie
     * @param MovieUpdateService $service
     * @return JsonResponse
     */
    public function update(UpdateMovieRequest $request, Movie $movie, MovieUpdateService $service): JsonResponse
    {
        $movieDto = $request->dto();

        $updated = $service
            ->update($movie, $movieDto);

        if($movieDto->hasCover()) {
            event(new CoverChanged($movie->cover, $request->resize()));
        }

        return new JsonResponse(
            MovieResource::make($updated)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyMovieRequest $request
     * @param Movie $movie
     * @param MovieDestroyService $service
     * @return JsonResponse
     */
    public function destroy(
        DestroyMovieRequest $request,
        Movie $movie,
        MovieDestroyService $service
    ): JsonResponse
    {
        $service
            ->destroy($movie);

        return new JsonResponse([
            'status' => 200
        ]);
    }

    /**
     * Search movies by title
     * For more sophisticated search we may consider Fulltext search, or dedicated search engine, ie:
     * - Elasticsearch
     * - Sphinx
     *
     *
     * @param SearchMoviesRequest $request
     * @param MovieSearchService $service
     * @return JsonResponse
     */
    public function search(SearchMoviesRequest $request, MovieSearchService $service): JsonResponse
    {
        return new JsonResponse(
            MovieResource::collection(
                $service->search($request->dto())
            )
        );
    }
}
