<?php


namespace App\Contracts\Models;


interface MovieModel extends Model
{
    /**
     * Assuming we've got here an abstraction for all Eloquent methods, like:
     * - create
     * - update
     * ...etc
     *
     * Leaving this interface blank since filling this with methods is not purpose of this task
     */

    public function hasCover(): bool;
}
