<?php

namespace Tests\Feature;

use App\Country;
use App\Genre;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieCreationTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @group api
     * @group movies
     * @dataProvider correctlyCreateMovieData
     */
    public function correctlyCreateMovie($movieTitle, $expectedTitle)
    {
        $country = factory(Country::class)->create();
        $genres = factory(Genre::class, 3)->create();

        $input = [
            'title' => $movieTitle,
            'description' => 'Example description',
            'country' => $country->id,
            'genres' => $genres
                ->pluck('id')
                ->toArray()
        ];

        $headers = [
            'Accept' => 'application/json',
            'Content-Type' => 'multipart/form-data'
        ];

        $response = $this->post(
            route('movies.store'),
            $input,
            $headers
        );

        $response->assertStatus(200);
        $response->assertJsonFragment([
            'title' => $expectedTitle
        ]);
    }

    public function correctlyCreateMovieData()
    {
        return [
            ['Test', 'Test']
        ];
    }
}
