<?php


namespace App\Dto\Cover;


use App\Contracts\Dtos\Dto;

class Resize implements Dto
{
    /**
     * @var int|null
     */
    private ?int $width;

    /**
     * @var int|null
     */
    private ?int $height;

    /**
     * @var bool|null
     */
    private ?bool $aspectRatio;

    /**
     * Resize constructor.
     * @param int|null $width
     * @param int|null $height
     * @param bool|null $aspectRatio
     */
    public function __construct(?int $width, ?int $height, ?bool $aspectRatio = true)
    {
        $this->width = $width;
        $this->height = $height;
        $this->aspectRatio = $aspectRatio;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @return bool|null
     */
    public function getAspectRatio(): ?bool
    {
        return $this->aspectRatio;
    }

    public function getAspectRatioFunction()
    {
        return $this->aspectRatio
            ?
                function($constraint) {
                    $constraint->aspectRatio();
                }
            :
                null;
    }
}
